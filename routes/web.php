<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});

Route::get('/about', function () {
    $nama = ' Wisang Jati Ismuwardoyo';
    return view('about', ['nama' => $nama]);
});

Route::get('/mahasiswa', function () {
    $dataMahasiswa = ['Wisang Jati Ismuwardoyo', 'Sistem Informasi', '2018', 'UB'];
    return view('mahasiswa', ['dataMahasiswa' => $dataMahasiswa]);
});
